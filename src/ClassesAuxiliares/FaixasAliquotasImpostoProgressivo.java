package ClassesAuxiliares;

/**
 * Created by Luiz Angel on 2/23/2016.
 */
public class FaixasAliquotasImpostoProgressivo {

    //*************************Atributos****************************
    //Faixa de piso
    private double _faixaPiso;

    //Faixa de teto
    private double _faixaTeto;

    //Aliquota do piso
    private double _aliquotaPiso;

    //Aliquota para valores entre o piso e o teto
    private double _aliquotaPisoTeto;

    //Aliquota para valores acima do teto
    private double _aliquotaTeto;


    //*************************Construtor***************************
    //Faixas diferentes
    public FaixasAliquotasImpostoProgressivo(double faixaPiso,
                                             double faixaTeto,
                                             double aliquotaPiso,
                                             double aliquotaPisoTeto,
                                             double aliquotaTeto) {

        _faixaPiso = faixaPiso;
        _faixaTeto = faixaTeto;

        _aliquotaPiso = aliquotaPiso;
        _aliquotaPisoTeto = aliquotaPisoTeto;
        _aliquotaTeto = aliquotaTeto;
    }

    //Faixas e aliquotas iguais
    protected FaixasAliquotasImpostoProgressivo(double faixaPisoETeto,
                                                double aliquotaPisoETeto) {

        _faixaPiso = faixaPisoETeto;
        _faixaTeto = faixaPisoETeto;

        _aliquotaPiso = aliquotaPisoETeto;
        _aliquotaPisoTeto = aliquotaPisoETeto;
        _aliquotaTeto = aliquotaPisoETeto;
    }


    //*************************Métodos******************************
    //Retorna a faixa piso
    public double getFaixaPiso() { return _faixaPiso; }

    //Retorna a faixa teto
    public double getFaixaTeto() { return _faixaTeto; }

    //Retorna a aliquota piso
    public double getAliquotaPiso() { return _aliquotaPiso; }

    //Retorna a aliquota entre o piso e o teto
    public double getAliquotaPisoTeto() { return _aliquotaPisoTeto; }

    //Retorna a aliquota teto
    public double getAliquotaTeto() { return _aliquotaTeto; }
}
