package ClassesAuxiliares;

/**
 * Created by Luiz Angel on 2/27/2016.
 */
public class AliquotaValorImposto {
    //************************Atributos****************************
    //Aliquota do Imposto (porcentagem)
    private double _aliquotaPorcentagem = 0;

    //Aliquota do Imposto (valor default fixo)
    private double _aliquotaDefaultFixo = 0;

    //Valor do imposto total
    private double _valorImposto = 0;

    //Categoria Tributaria do Imposto
    private CategoriasTributarias _categoriaTributaria;

    //*************************Construtor***************************
    public AliquotaValorImposto(double aliquotaPorcentagem,
                                double aliquotaDefaultFixo,
                                double valorImposto,
                                CategoriasTributarias categoria) {

        _aliquotaPorcentagem = aliquotaPorcentagem;
        _aliquotaDefaultFixo = aliquotaDefaultFixo;
        _valorImposto = valorImposto;
        _categoriaTributaria = categoria;
    }

    //*************************Métodos******************************
    //Retorna a aliquota (porcentagem)
    public double getAliquotaPorcentagem() { return _aliquotaPorcentagem; }

    //Retorna a aliquota (default)
    public double getAliquotaDefaultFixo() { return _aliquotaDefaultFixo; }

    //Retorna o valor do Imposto
    public double getValorImposto() { return _valorImposto; }

    //Retorna a categora tributaria do imposto
    public CategoriasTributarias getCategoriaTributaria() { return _categoriaTributaria; }
}