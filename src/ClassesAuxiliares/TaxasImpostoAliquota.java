package ClassesAuxiliares;

/**
 * Created by Luiz Angel on 2/23/2016.
 */
public class TaxasImpostoAliquota {

    //*************************Atributos****************************
    //Taxa do tipo percentual
    private double _taxaPorcentagem;

    //Taxa do tipo valor fixo por IV
    private double _taxaDefaultIV;


    //*************************Construtor***************************
    public TaxasImpostoAliquota(double taxaPorcentagem,
                                double taxaDefaultIV) {

        _taxaPorcentagem = taxaPorcentagem;
        _taxaDefaultIV = taxaDefaultIV;
    }


    //*************************Métodos******************************
    //Retorna a taxa do tipo percentual
    public double getTaxaPorcentagem() { return _taxaPorcentagem; }

    //Retorna a taxa do tipo valor fixo for IV
    public double getTaxaDefaultIV() { return _taxaDefaultIV; }
}