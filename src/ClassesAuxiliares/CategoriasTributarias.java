package ClassesAuxiliares;

/**
 * Created by Luiz Angel on 2/22/2016.
 */
public enum CategoriasTributarias {

    RendaPessoaFisica,
    RendaPessoaJuridica,
    Habitacao,
    Transporte,
    Previdencia,
    Educacao,
    Veiculo,
    Importacao,
    Exportacao,
    Alimentacao,
    Vestimenta
}
