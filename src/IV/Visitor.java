package IV;

import ClassesAuxiliares.AliquotaValorImposto;
import Imposto.Imposto;

import java.util.List;

/**
 * Created by Luiz Angel on 2/27/2016.
 */
public interface Visitor {
    public double visit(ItemVenda itemVenda, List<String> dados);

    public void visit(Imposto imposto, AliquotaValorImposto aliquotaValorImposto);
}
