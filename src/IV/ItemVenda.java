package IV;

import ClassesAuxiliares.AliquotaValorImposto;
import ClassesAuxiliares.CategoriasTributarias;
import Imposto.Imposto;

import java.util.*;

/**
 * Created by Luiz Angel on 2/22/2016.
 */
public class ItemVenda implements Visitor {

    //************************Atributos****************************
    //Nome do IV
    private String _nomeIV;

    //Valor do IV
    private double _valorIV = 0;

    //Categoria Tributária
    private CategoriasTributarias _categTributIV;

    //IVs
    private List<ItemVenda> _itensVenda;

    //Dicionario de todos os impostos cobrados para o Item de Venda
    private Dictionary<Imposto, AliquotaValorImposto> _valoresImpostosCobrados = new Hashtable<>();


    //*************************Construtor***************************
    //Construtor sem categoria tributaria
    private ItemVenda() { }


    //*************************Métodos******************************
    //Criar IV apenas com o nome
    private static ItemVenda canCreateItemVenda(String nomeIV, double valorIV) {
        try {
            if(nomeIV != null) {
                ItemVenda itemVenda = new ItemVenda();
                itemVenda._nomeIV = nomeIV;
                itemVenda._categTributIV = null;
                itemVenda._valorIV = valorIV;
                itemVenda._itensVenda = new ArrayList<ItemVenda>();
                return itemVenda;
            }
            else {
                throw new Exception("Item de Venda nao criada com sucesso, pois o nome e' nulo");
            }
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    //Criar IV apenas com nome e categoria tributaria
    public static ItemVenda createItemVenda(String nomeIV, CategoriasTributarias categTribut, double valorIV) {
        ItemVenda itemVenda = ItemVenda.canCreateItemVenda(nomeIV, valorIV);
        if(itemVenda != null) {
            itemVenda._categTributIV = categTribut;
        }
        return itemVenda;
    }

    //Criar IV com nome, categoria tributaria e IVs
    public static ItemVenda createItemVenda(String nomeIV,
                                            CategoriasTributarias categTribut,
                                            ArrayList<ItemVenda> itensVenda,
                                            double valorIV) {
        ItemVenda itemVenda = ItemVenda.canCreateItemVenda(nomeIV, valorIV);
        if(itemVenda != null) {
            itemVenda._categTributIV = categTribut;
            if(itensVenda != null) {
                itemVenda._itensVenda = itensVenda;
            }
        }
        return itemVenda;
    }


    //Adiciona IVs a lista de IVs
    public void addItemVenda(ItemVenda IV) {
        try {
            if (IV != null) {
                _itensVenda.add(IV);
            } else {
                throw new Exception("Item de Venda nao foi adicionado com sucesso, pois tal Item de Venda e null");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    //Retorna o nome do IV
    public String getNomeIV() { return _nomeIV; }

    //Retorna o valor do IV
    public double getValorIV() { return _valorIV; }

    //Retorna a categoria tributaria do IV
    public CategoriasTributarias getCategTribut() { return _categTributIV; }

    //Retorna a lista de IVs
    public List<ItemVenda> getItensVenda() { return Collections.unmodifiableList(_itensVenda); }

    //Retorna o dicionario de impostos cobrados para este IV
    public Dictionary<Imposto, AliquotaValorImposto> getValoresImpostosCobrados() { return _valoresImpostosCobrados; }

    //Modifica a categoria tributaria do IV
    public void setCategTribut(CategoriasTributarias categ) {
        try {
            if (categ != null) {
                _categTributIV = categ;
            } else {
                System.out.println("Categoria Tributaria do IV " + _nomeIV + " nao foi modificada com sucesso");
            }
        } catch (Exception e) {
            System.out.println("Item de Venda inicializado incorretamente");
        }
    }

    //Adiciona valores de impostos cobrados para este IV
    public void addValorImposto(Imposto Imposto, AliquotaValorImposto aliquotaValorImposto) {
        if(Imposto != null) {
            _valoresImpostosCobrados.remove(Imposto);
            _valoresImpostosCobrados.put(Imposto, aliquotaValorImposto);
        }
    }



    //Visita um visitor (no caso, visita um imposto)
    @Override
    public double visit(ItemVenda itemVenda, List<String> dados) {
        double valorIVs = 0;
        for(ItemVenda iv : itemVenda.getItensVenda()) {
            valorIVs += iv.getValorIV() + iv.visit(iv, dados);
        }
        return valorIVs;
    }

    @Override
    public void visit(Imposto imposto, AliquotaValorImposto aliquotaValorImposto) {
        addValorImposto(imposto, aliquotaValorImposto);
    }
}
