package NF;

import java.util.*;

/**
 * Created by Luiz Angel on 2/21/2016.
 */

//Creational DP: Singleton
public class BancoNotasFiscais {

    //************************Atributos****************************
    //Singleton - única instância de BancoNotasFiscais (BNF)
    private static final BancoNotasFiscais _uniqueINSTANCE = new BancoNotasFiscais();

    //Contador para gerar ID único
    private int _IDgenerator = -1;

    //Lista de notas fiscais
    private List<NotaFiscal> _notasFiscais = new ArrayList<NotaFiscal>();

    //*************************Construtor***************************
    private BancoNotasFiscais() {}


    //*************************Métodos******************************
    //Método para retornar a única instância
    public static BancoNotasFiscais getInstanceOfBancodeNotasFiscais() { return _uniqueINSTANCE; }

    //Método para gerar IDs das NFs
    protected int gerarID() {
        _IDgenerator ++;
        return _IDgenerator;
    }

    //Método para adicionar uma NF à lista de NFs do BNF
    protected void addNotaFiscalToBNF (NotaFiscal nf) {
       _uniqueINSTANCE._notasFiscais.add(nf);
    }

    //Método que retorna a lista de NFs do BNF
    public List<NotaFiscal> getNotasFiscais() { return Collections.unmodifiableList(_notasFiscais); }

    //Método para limpar a lista de NFs
    public void clearBNF () { _notasFiscais.clear(); }
}
