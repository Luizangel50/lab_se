package NF;

import IV.ItemVenda;

import java.util.ArrayList;

/**
 * Created by Luiz Angel on 2/21/2016.
 */

//Creational DP: Builder
public class NotaFiscalBuilder {

    ///************************Atributos****************************


    //*************************Construtor***************************
    private NotaFiscalBuilder() {}


    //*************************Métodos******************************
    //Método para criar uma NF e adicioná-la ao BNF, retornando a NF criada, sem IVs
    public static NotaFiscal createNewNotaFiscal() {
        int ID = BancoNotasFiscais.getInstanceOfBancodeNotasFiscais().gerarID();
        NotaFiscal notaFiscal = new NotaFiscal(ID);
        BancoNotasFiscais.getInstanceOfBancodeNotasFiscais().addNotaFiscalToBNF(notaFiscal);
        return notaFiscal;
    }

    //Método para criar uma NF e adicioná-la ao BNF, retornando a NF criada, com IVs
    public static NotaFiscal createNewNotaFiscal(ArrayList<ItemVenda> ItensVenda) {
        if(ItensVenda != null && !ItensVenda.isEmpty()) {
            int ID = BancoNotasFiscais.getInstanceOfBancodeNotasFiscais().gerarID();
            NotaFiscal notaFiscal = new NotaFiscal(ID, ItensVenda);
            BancoNotasFiscais.getInstanceOfBancodeNotasFiscais().addNotaFiscalToBNF(notaFiscal);
            return notaFiscal;
        }
        else {
            System.out.println("Nota Fiscal nao criada com sucesso, pois a lista de Itens de Venda esta' nula ou vazia");
            return null;
        }
    }
}
