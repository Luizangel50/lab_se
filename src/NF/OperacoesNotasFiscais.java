package NF;

import ClassesAuxiliares.AliquotaValorImposto;
import ClassesAuxiliares.CategoriasTributarias;
import ClassesAuxiliares.FaixasAliquotasImpostoProgressivo;
import IV.ItemVenda;
import Imposto.BancoImpostos;
import Imposto.Imposto;
import Imposto.ImpostoAliquota;
import Imposto.ImpostoProgressivo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luiz Angel on 2/24/2016.
 */

public class OperacoesNotasFiscais {
    //************************************************Atributos************************************************
    //Banco de Impostos
    private static BancoImpostos bancoImpostos = BancoImpostos.getInstanceOfBancodeImpostos();

    //Banco de Notas Fiscais
    private static BancoNotasFiscais bancoNotasFiscais = BancoNotasFiscais.getInstanceOfBancodeNotasFiscais();

    //Soma dos valores dos IVs de todas as NFs vendidas
    private static double _valorTotalIVsDeNFsVendidas = 0;
    

    //************************************************Construtor************************************************
    private OperacoesNotasFiscais() {}


    //************************************************Métodos************************************************

    public static void calcularImpostoTodasNFs() {
        for(NotaFiscal nf : bancoNotasFiscais.getNotasFiscais()) {
            calcularImposto(nf);
        }
    }

    //*************************Métodos para calcular Impostos******************************
    //Metodo que calcula o valor da NF junto com os seus impostos
    private static void calcularImposto(NotaFiscal NF) {
        try {
            for (Imposto imposto : bancoImpostos.getImpostos()) {
                imposto.setValorPago(0.0);
                NF.changeInformacoesImpostos(imposto, "");
                if (imposto.getTipoImposto().equals("ALIQUOTA")) {
                    boolean hasCategoria = false;
                    for (CategoriasTributarias categ : CategoriasTributarias.values()) {
                        if (((ImpostoAliquota) imposto).getTaxas().get(categ) != null) {
                            hasCategoria = true;
                            for (ItemVenda item : NF.getItensVenda()) {
                                calcularImpostoAliquota(NF, item, categ, (ImpostoAliquota) imposto);
                            }
                        }
                    }
                    if(hasCategoria) {
                        double taxaFixaNF = ((ImpostoAliquota) imposto).getTaxaFixaNF();
                        imposto.addValorFixoPorNF(NF.getID(), taxaFixaNF);
                        imposto.changeValorPago(taxaFixaNF);
                        if(taxaFixaNF != 0) {
                            String info = NF.getInformacoesImpostos().get(imposto);
                            info += "\n\nValor fixo por Nota Fiscal: RS " + taxaFixaNF;
                            NF.changeInformacoesImpostos(imposto, info);
                        }
                    }
                } else if (imposto.getTipoImposto().equals("PROGRESSIVO")) {
                    for (CategoriasTributarias categ : ((ImpostoProgressivo) imposto).getListaCategTribut()) {
                        for (ItemVenda item : NF.getItensVenda()) {
                            calcularImpostoProgressivo(NF, item, categ, ((ImpostoProgressivo) imposto));
                        }
                    }
                }
                if (imposto.getValorPago() != 0.0) {
                    String info = NF.getInformacoesImpostos().get(imposto);
                    info += "\nValor pago para este imposto: RS " + imposto.getValorPago();
                    NF.changeInformacoesImpostos(imposto, info);
                }
            }
            venderNF(NF);
        }
        catch(Exception e) {
            System.out.println("Nota Fiscal nao foi buscada com sucesso.");
        }
    }

    //Metodo que visita os IVs internos a fim de calcular o valor
    //dos IVs para impostos do tipo ALIQUOTA
    private static void calcularImpostoAliquota(NotaFiscal nf,
                                                ItemVenda itemVenda,
                                                CategoriasTributarias categTribut,
                                                ImpostoAliquota imposto) {

        double porcentagem = imposto.getTaxas().get(categTribut).getTaxaPorcentagem();
        double taxaDefaultIV = imposto.getTaxas().get(categTribut).getTaxaDefaultIV();
        if(itemVenda.getCategTribut() == categTribut) {
            String info = nf.getInformacoesImpostos().get(imposto);
            info += "\n\n****Imposto do tipo ALIQUOTA: " + imposto.getNomeImposto() + "****";
            if(porcentagem != 0) info += "\nAliquota - Porcentagem: " + porcentagem + "%";
            if(taxaDefaultIV != 0) info += "\nAliquota - Valor default: RS " + taxaDefaultIV;
            info += "\nCategoria Tributaria: " + categTribut.name();
            nf.changeInformacoesImpostos(imposto, info);
            imposto.changeValorPago(itemVenda.getValorIV()*(porcentagem/100) + taxaDefaultIV);
            imposto.acceptVisitor(itemVenda,
                    new AliquotaValorImposto(porcentagem, taxaDefaultIV, itemVenda.getValorIV()*(porcentagem/100) + taxaDefaultIV, categTribut));
            //if(!nf.isVendida()) _valorTotalIVsDeNFsVendidas += itemVenda.getValorIV();
        }
        for(ItemVenda item : itemVenda.getItensVenda()) {
            calcularImpostoAliquota(nf, item, categTribut, imposto);
        }
    }

    //Metodo que ivisita os IVs internos a fim de calcular o valor
    //dos IVs para impostos do tipo PROGRESSIVO
    private static void calcularImpostoProgressivo(NotaFiscal nf,
                                                   ItemVenda itemVenda,
                                                   CategoriasTributarias categTribut,
                                                   ImpostoProgressivo imposto) {

        FaixasAliquotasImpostoProgressivo faixas = imposto.getFaixasAliquotas();
        if(itemVenda.getCategTribut() == categTribut) {
            String info = nf.getInformacoesImpostos().get(imposto);
            info += "\n\n****Imposto do tipo PROGRESSIVO: " + imposto.getNomeImposto() + "****";
            if(_valorTotalIVsDeNFsVendidas <= faixas.getFaixaPiso()) {
                imposto.changeValorPago(itemVenda.getValorIV()*(faixas.getAliquotaPiso()/100));
                imposto.acceptVisitor(itemVenda,
                        new AliquotaValorImposto(faixas.getAliquotaPiso(),0, itemVenda.getValorIV()*(faixas.getAliquotaPiso()/100), categTribut));
                info += "\nAliquota - Porcentagem: " + faixas.getAliquotaPiso() + "%";
                info += "\nCategoria Tributaria: " + categTribut.name();
                nf.changeInformacoesImpostos(imposto, info);
            }
            else if(_valorTotalIVsDeNFsVendidas > faixas.getFaixaPiso()
                    && _valorTotalIVsDeNFsVendidas <= faixas.getFaixaTeto()) {

                imposto.changeValorPago(itemVenda.getValorIV()*(faixas.getAliquotaPisoTeto()/100));
                imposto.acceptVisitor(itemVenda,
                        new AliquotaValorImposto(faixas.getAliquotaPisoTeto(),0, itemVenda.getValorIV()*(faixas.getAliquotaPisoTeto()/100), categTribut));
                info += "\nAliquota - Porcentagem: " + faixas.getAliquotaPisoTeto() + "%";
                info += "\nCategoria Tributaria: " + categTribut.name();
                nf.changeInformacoesImpostos(imposto, info);
            }
            else if(_valorTotalIVsDeNFsVendidas <= faixas.getFaixaTeto()) {
                imposto.changeValorPago(itemVenda.getValorIV()*(faixas.getAliquotaTeto()/100));
                imposto.acceptVisitor(itemVenda,
                        new AliquotaValorImposto(faixas.getAliquotaTeto(),0, itemVenda.getValorIV()*(faixas.getAliquotaTeto()/100), categTribut));
                info += "\nAliquota - Porcentagem: " + faixas.getAliquotaTeto() + "%";
                info += "\nCategoria Tributaria: " + categTribut.name();
                nf.changeInformacoesImpostos(imposto, info);
            }
//            if(!nf.isVendida()) _valorTotalIVsDeNFsVendidas += itemVenda.getValorIV();
        }
        for(ItemVenda item : itemVenda.getItensVenda()) {
            calcularImpostoProgressivo(nf, item, categTribut, imposto);
        }
    }




    //Metodo que permite a NF ser vendida
    private static List<String> venderNF(NotaFiscal nf) {
        List<String> dados = new ArrayList<>();
        double valorIVs = 0;
        for(ItemVenda itemVenda : nf.getItensVenda()) {
            valorIVs += itemVenda.getValorIV() + itemVenda.visit(itemVenda, dados);
        }
        dados.add("\n*****************Impostos*****************");
        double[] valorImpostos = percorrerDadosImpostos(nf, dados);

        //Vender NF
        if(!nf.isVendida()) _valorTotalIVsDeNFsVendidas += valorIVs;
        nf.venderNF(valorIVs + valorImpostos[valorImpostos.length-1] + valorImpostos[valorImpostos.length-2], valorIVs, valorImpostos);
        return  dados;
    }

    //Percorre os impostos do Banco de Impostos para obter informacoes
    //de aliquotas cobradas para as NFs
    private static double[] percorrerDadosImpostos(NotaFiscal nf, List<String> dados) {
        double[] valorImpostos = new double[bancoImpostos.getImpostos().size() + 2];
        for(Imposto imposto : bancoImpostos.getImpostos()) {
            String info = nf.getInformacoesImpostos().get(imposto);
            if(info != null) {
                dados.add(info);
            }

            for(ItemVenda itemVenda : nf.getItensVenda()) {
                valorImpostos[bancoImpostos.getImpostos().indexOf(imposto)] += percorrerIVs(itemVenda, imposto, new ArrayList<String>());
            }

            if(imposto.getValoresImpostosFixosCobrados().get(nf.getID()) != null) {
                //Impostos fixos
                valorImpostos[bancoImpostos.getImpostos().size()] += imposto.getValoresImpostosFixosCobrados().get(nf.getID());
            }

            //Valor total dos Impostos sem os impostos fixos por NF
            valorImpostos[bancoImpostos.getImpostos().size()+1] += valorImpostos[bancoImpostos.getImpostos().indexOf(imposto)];
        }
        //valorImpostos[bancoImpostos.getImpostos().size()+1] += valorImpostos[bancoImpostos.getImpostos().size()];
        return valorImpostos;
    }


    //Metodo auxiliar para executar passos recursivos, a fim de captar informacoes sobre um
    //imposto aplicado em IVs (Requisitos 9 e 10)
    private static double percorrerIVs(ItemVenda itemVenda, Imposto imposto, List<String> dados) {
        double valorImpostos = 0;
        String informacaoIV = "";

        //Captar informacoes do Item de Venda
        informacaoIV += "| Item de Venda: " + itemVenda.getNomeIV() + " | ";
        if(itemVenda.getCategTribut() != null) informacaoIV += "Categoria: " + itemVenda.getCategTribut().name();
        informacaoIV += " | Custo do IV: RS " + itemVenda.getValorIV();

        //dados.add(informacaoIV);
        AliquotaValorImposto aliquotaValorImposto = itemVenda.getValoresImpostosCobrados().get(imposto);

        //Somar impostos para obter o total de impostos pagos
        if(aliquotaValorImposto != null) {
            valorImpostos += aliquotaValorImposto.getValorImposto();
        }

        //percorrer todos os impostos
        for (Imposto impostoDoBI : bancoImpostos.getImpostos()) {
            aliquotaValorImposto = itemVenda.getValoresImpostosCobrados().get(impostoDoBI);
            if (aliquotaValorImposto != null) {
                informacaoIV += " | Valor para Imposto " + impostoDoBI.getNomeImposto() + ": RS " + aliquotaValorImposto.getValorImposto();
            }
        }
        dados.add(informacaoIV);


        //Recursao
        for(ItemVenda iv : itemVenda.getItensVenda()) {
            aliquotaValorImposto = iv.getValoresImpostosCobrados().get(imposto);
            valorImpostos += percorrerIVs(iv, imposto, dados);
        }
        return valorImpostos;
    }




    //*************************Métodos utilizados pelo Model******************************
    //Buscar dados da NF com detalhes para os impostos
    //de acordo com o requisito 9
    public static List<String> buscarDetalhesNFeImpostos(NotaFiscal nf) {
        List<String> dados = venderNF(nf);
        return dados;
    }




    //Buscar dados da NF com detalhes para os Itens de Venda
    //de acordo com o requisito 10
    public static void buscarDetalhesNFeIVs(NotaFiscal nf, List<String> dados) {
        try {
            for (ItemVenda itemVenda : nf.getItensVenda()) {
                percorrerIVs(itemVenda, bancoImpostos.getImpostos().get(0), dados);
            }
            String dadosImpostosIVs = "";
            dadosImpostosIVs += "Custo Total dos IVs: RS " + nf.getValorIVs() + " | ";
            for (Imposto imposto : bancoImpostos.getImpostos()) {
                int index = bancoImpostos.getImpostos().indexOf(imposto);
                if (nf.getValoresImpostos()[index] != 0) {
                    dadosImpostosIVs += "Valor Total para o Imposto " + imposto.getNomeImposto() + ": RS " + nf.getValoresImpostos()[index] + " | ";
                }
            }
            dados.add(dadosImpostosIVs);
        }
        catch (Exception e) {
            System.out.println("Nota Fiscal nao escontrada");
        }
    }




    //Lista todas as NFs do sistema com informacoes superficiais
    //de acordo com o requisito 11
    public static double[][] buscarInformacoesSuperficiaisDeNFs() {
        double[][] dados = new double[bancoNotasFiscais.getNotasFiscais().size()][5];
        for(NotaFiscal nf : bancoNotasFiscais.getNotasFiscais()) {
            int tamanho = nf.getValoresImpostos().length;
            int index = bancoNotasFiscais.getNotasFiscais().indexOf(nf);
            dados[index][0] = nf.getID();
            dados[index][1] = nf.getValorIVs();
            dados[index][2] = nf.getValoresImpostos()[tamanho - 1];
            dados[index][3] = nf.getValoresImpostos()[tamanho - 2];
            dados[index][4] = nf.getValorNF();
        }
        return dados;
    }




    //*************************Métodos getter e setter******************************
    //Metodo que retorna o valor no acumulador
    public static double getValorAcumulador() { return _valorTotalIVsDeNFsVendidas; }

    //Metodo que zera o acumulador
    protected static void zerarAcumuladorValorIVs() {
        _valorTotalIVsDeNFsVendidas = 0;
    }

}
