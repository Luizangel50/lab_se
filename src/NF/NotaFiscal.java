package NF;

import IV.ItemVenda;
import Imposto.Imposto;

import java.util.*;

/**
 * Created by Luiz Angel on 2/21/2016.
 */
public class NotaFiscal {

    //************************Atributos****************************
    //ID unico
    private int _ID;

    //Lista de IVs
    private List<ItemVenda> _itensVenda;

    //NF vendida ou nao
    private boolean _vendida;

    //Valor da Nota Fiscal
    private double _precoNF = 0;

    //Valor dos IVs
    private double _precoIVs = 0;

    //Valor impostos sobre os IVs
    private double[] _valoresImpostos;

    //Dicionario com informacoes sobre os impostos aplicados a esta NF
    private Dictionary<Imposto, String> _informacoesImpostos = new Hashtable<>();


    //*************************Construtor***************************
    //Construtor sem IVs
    protected NotaFiscal (int ID) {
        this._ID = ID;
        _itensVenda = new ArrayList<ItemVenda>();
        _vendida = false;
    }

    //Construtor com IVs
    protected NotaFiscal(int ID, ArrayList<ItemVenda> IVs) {
        _ID = ID;
        _itensVenda = IVs;
        _vendida = false;
    }


    //*************************Métodos******************************
    //Retorna o ID da NF
    public int getID() {
        try {
            return _ID;
        }
        catch (Exception e) {
            System.out.println("Nota Fiscal nao criada");
            return -1;
        }
    }

    //Retorna a lista de IVs
    public List<ItemVenda> getItensVenda() { return Collections.unmodifiableList(_itensVenda); }

    //Retorna o valor da NF
    public double getValorNF() { return _precoNF; }

    //Retorna o valor da NF
    public double getValorIVs() { return _precoIVs; }

    //Retorna o vetor de valores de Impostos cobrados
    public double[] getValoresImpostos() { return _valoresImpostos.clone(); }

    //Retorna se a NF foi vendida ou nao
    public boolean isVendida() { return _vendida; }

    //Retorna o dicionario de informacoes sobre impostos
    protected Dictionary<Imposto, String> getInformacoesImpostos() {
        return _informacoesImpostos;
    }

    //Adiciona ou modifica dados do dicionario de informacoes sobre impostos
    protected void changeInformacoesImpostos(Imposto imposto, String informacoes) {
        if(imposto != null) {
            _informacoesImpostos.remove(imposto);
            _informacoesImpostos.put(imposto, informacoes);
        }
    }

    //Vender uma NF
    protected void venderNF(double valorNF, double valorIVs, double[] valoresImpostos) {
        if (!_vendida) {
            _vendida = true;
            _precoNF = valorNF;
            _precoIVs = valorIVs;
            _valoresImpostos = valoresImpostos;
        }
    }
}
