package Imposto;

import ClassesAuxiliares.CategoriasTributarias;
import ClassesAuxiliares.FaixasAliquotasImpostoProgressivo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Luiz Angel on 2/22/2016.
 */
public class ImpostoProgressivo extends Imposto {

    //*************************Atributos****************************
    //Lista de Categorias Tributárias em que este imposto se aplica
    private List<CategoriasTributarias> _listaCategTribut = new ArrayList<CategoriasTributarias>();

    //Lista de faixas de valores e aliquotas
    private FaixasAliquotasImpostoProgressivo _faixasAliquotasImpostoProgressivo;


    //*************************Construtor***************************
    protected ImpostoProgressivo(String nomeImpostoProgressivo,
                                 FaixasAliquotasImpostoProgressivo faixasAliquotasImpostoProgressivo) {

        super();
        _tipoImposto = "PROGRESSIVO";
        _nomeImposto = nomeImpostoProgressivo;
        _faixasAliquotasImpostoProgressivo = faixasAliquotasImpostoProgressivo;
    }


    //*************************Métodos******************************
    //Metodo que adiciona Categorias Tributarias
    public void addCategTribut(CategoriasTributarias categTribut) {
        if(!_listaCategTribut.contains(categTribut)) {
            _listaCategTribut.add(categTribut);
        }
    }

    //Retorna a faixa de aliquotas
    public FaixasAliquotasImpostoProgressivo getFaixasAliquotas() {
        return _faixasAliquotasImpostoProgressivo;
    }

    //Retorna a lista de Categorias Tributarias
    public List<CategoriasTributarias> getListaCategTribut() {
        return Collections.unmodifiableList(_listaCategTribut);
    }

}
