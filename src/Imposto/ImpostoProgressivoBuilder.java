package Imposto;

import ClassesAuxiliares.FaixasAliquotasImpostoProgressivo;

/**
 * Created by Luiz Angel on 2/23/2016.
 */
public class ImpostoProgressivoBuilder {

    //*************************Atributos****************************


    //*************************Construtor***************************
    private ImpostoProgressivoBuilder() {}


    //*************************Métodos******************************
    //Cria um imposto do tipo PROGRESSIVO e retorna o imposto criado
    public static ImpostoProgressivo createImpostoProgressivo(String nome,
                                                              double faixaPiso,
                                                              double faixaTeto,
                                                              double aliquotaPiso,
                                                              double aliquotaPisoTeto,
                                                              double aliquotaTeto) {

        boolean canCreate = canCreateImpostoProgressivo(nome,
                faixaPiso,
                faixaTeto,
                aliquotaPiso,
                aliquotaPisoTeto,
                aliquotaTeto);

        if(canCreate) {
            FaixasAliquotasImpostoProgressivo faixasAliquotasImpostoProgressivo =
                    new FaixasAliquotasImpostoProgressivo(faixaPiso,
                            faixaTeto,
                            aliquotaPiso,
                            aliquotaPisoTeto,
                            aliquotaTeto);
            ImpostoProgressivo impostoProgressivo = new ImpostoProgressivo(nome, faixasAliquotasImpostoProgressivo);
            return impostoProgressivo;
        }
        else {
            System.out.println("Imposto do tipo PROGRESSIVO nao foi criado com sucesso");
            return null;
        }
    }

    //Cria um imposto do tipo PROGRESSIVO com faixas iguais
    public static ImpostoProgressivo createImpostoProgressivo(String nome,
                                                              double faixaPisoETeto,
                                                              double aliquotaPisoETeto) {

        boolean canCreate = canCreateImpostoProgressivo(nome, faixaPisoETeto, aliquotaPisoETeto);
        if(canCreate) {
            FaixasAliquotasImpostoProgressivo faixasAliquotasImpostoProgressivo =
                    new FaixasAliquotasImpostoProgressivo(faixaPisoETeto,
                            faixaPisoETeto,
                            aliquotaPisoETeto,
                            aliquotaPisoETeto,
                            aliquotaPisoETeto);
            ImpostoProgressivo impostoProgressivo = new ImpostoProgressivo(nome, faixasAliquotasImpostoProgressivo);
            return impostoProgressivo;
        }
        else {
            System.out.println("Imposto do tipo PROGRESSIVO nao foi criado com sucesso, pois o nome do Imposto nao pode ser null");
            return null;
        }
    }

    //Avalia se os dados para criacao do ImpostoProgressivo estao coerentes
    //(nome nao pode null) e (faixaPiso deve ser menor que faixaTeto ou
    //(faixaPiso = faixaTeto e todas as aliquotas devem iguais))
    private static boolean canCreateImpostoProgressivo(String nome,
                                                       double faixaPiso,
                                                       double faixaTeto,
                                                       double aliquotaPiso,
                                                       double aliquotaPisoTeto,
                                                       double aliquotaTeto) {

        if(     nome != null
                && (faixaPiso < faixaTeto
                || (faixaPiso == faixaTeto
                && aliquotaPiso == aliquotaPisoTeto
                && aliquotaPisoTeto == aliquotaTeto))) {

            return true;
        }
        else { return false; }
    }

    //Faz a mesmo papel do metodo acima,
    //porem as faixas e aliquotas sao iguais
    private static boolean canCreateImpostoProgressivo(String nome,
                                                       double faixaPisoETeto,
                                                       double aliquotaPisoETeto) {

        if(nome != null && faixaPisoETeto == faixaPisoETeto) { return true; }
        else { return false; }
    }
}
