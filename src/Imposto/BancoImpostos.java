package Imposto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Luiz Angel on 2/23/2016.
 */

//Creational DP: Singleton
public class BancoImpostos {

    //************************Atributos****************************
    //Singleton - única instância de BancoImpostos (BI)
    private static final BancoImpostos _uniqueINSTANCE = new BancoImpostos();

    //Lista de impostos
    private List<Imposto> _impostos = new ArrayList<Imposto>();


    //*************************Construtor***************************
    private BancoImpostos() {}


    //*************************Métodos******************************
    //Método para retornar a única instância
    public static BancoImpostos getInstanceOfBancodeImpostos() { return _uniqueINSTANCE; }

    //Método para adicionar um Imposto à lista de Impostos do BI
    protected void addImpostoToBI (Imposto imposto) {
        _impostos.add(imposto);
    }

    //Método que retorna a lista de Impostos do BI
    public List<Imposto> getImpostos() { return Collections.unmodifiableList(_impostos); }

    //Método para limpar a lista de Impostos
    public void clearBI () {
        _impostos.clear();
    }
}
