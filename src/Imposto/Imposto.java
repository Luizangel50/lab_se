package Imposto;

import IV.Visitor;
import ClassesAuxiliares.AliquotaValorImposto;

import java.util.Dictionary;
import java.util.Hashtable;

/**
 * Created by Luiz Angel on 2/22/2016.
 */
public abstract class Imposto  {

    //************************Atributos****************************
    //Tipo de Imposto
    protected String _tipoImposto;

    //Nome do Imposto
    protected String _nomeImposto;

    //Valor pago para este imposto por uma NF
    protected double _valorPago = 0;

    //Dicionario de todos os impostos cobrados para o Item de Venda
    private Dictionary<Integer, Double> _valoresImpostosFixosCobrados = new Hashtable<>();


    //*************************Construtor***************************
    protected Imposto() {
        BancoImpostos.getInstanceOfBancodeImpostos().addImpostoToBI(this);
    }


    //*************************Métodos******************************
    //Retorna o tipo de imposto
    public String getTipoImposto() {return _tipoImposto;}

    //Retorna o nome do imposto
    public String getNomeImposto() {return _nomeImposto;}

    //Retorna o valor pago
    public double getValorPago() { return _valorPago; }

    //Retorna o dicionario de impostos fixos cobrados por este imposto
    public Dictionary<Integer, Double> getValoresImpostosFixosCobrados() { return _valoresImpostosFixosCobrados; }

    //Modifica o valor pago
    public void changeValorPago(double valorPago) { _valorPago += valorPago; }

    //Modifica o valor pago
    public void setValorPago(double valorPago) { _valorPago = valorPago; }

    //Adiciona os impostos fixos cobrados para uma NF
    public void addValorFixoPorNF(int idNotaFiscal, double valorImposto) {
        if(_valoresImpostosFixosCobrados.get(idNotaFiscal) == null) {
            _valoresImpostosFixosCobrados.put(idNotaFiscal, valorImposto);
        }
    }

    //Aceita o visitor
    public void acceptVisitor(Visitor visitor, AliquotaValorImposto aliquotaValorImposto) {
        visitor.visit(this, aliquotaValorImposto);
    }

}
