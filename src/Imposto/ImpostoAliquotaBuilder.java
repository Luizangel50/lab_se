package Imposto;

import ClassesAuxiliares.CategoriasTributarias;

/**
 * Created by Luiz Angel on 2/23/2016.
 */
public class ImpostoAliquotaBuilder {

    //*************************Atributos****************************


    //*************************Construtor***************************
    private ImpostoAliquotaBuilder() {}


    //*************************Métodos******************************
    //Cria um imposto do tipo ALIQUOTA e retorna o imposto criado
    public static ImpostoAliquota createImpostoAliquota(String nome,
                                                        double taxaFixaNF) {

        if(nome != null) {
            ImpostoAliquota impostoAliquota = new ImpostoAliquota(nome, taxaFixaNF);
            return impostoAliquota;
        }
        else {
            System.out.println("Imposto do tipo ALIQUOTA nao foi criado com sucesso, pois o nome do Imposto nao pode ser null");
            return null;
        }
    }

    //Cria um imposto do tipo ALIQUOTA e retorna o imposto criado
    public static ImpostoAliquota createImpostoAliquota(String nome,
                                                        double taxaFixaNF,
                                                        CategoriasTributarias categTribut,
                                                        double taxaPorcentagem,
                                                        double taxaDefaultIV) {

        if(nome != null && categTribut != null) {
            ImpostoAliquota impostoAliquota = new ImpostoAliquota(nome, taxaFixaNF);
            impostoAliquota.addOrChangeAliquota(categTribut, taxaPorcentagem, taxaDefaultIV);
            return impostoAliquota;
        }
        else {
            System.out.println("Imposto do tipo ALIQUOTA nao foi criado com sucesso");
            return null;
        }
    }
}
