package Imposto;

import ClassesAuxiliares.CategoriasTributarias;
import ClassesAuxiliares.TaxasImpostoAliquota;

import java.util.Dictionary;
import java.util.Hashtable;

/**
 * Created by Luiz Angel on 2/22/2016.
 */
public class ImpostoAliquota extends Imposto {

    //*************************Atributos****************************
    //Aliquotas
    private Dictionary<CategoriasTributarias, TaxasImpostoAliquota> _taxas;

    //Taxa do tipo valor fixo por NF
    private double _taxaFixaNF;


    //*************************Construtor***************************
    //ImpostoAliquota com algumas taxas definidas
    protected ImpostoAliquota(String nomeImpostoAliquota,
                              double taxaFixaNF) {

        super();
        _tipoImposto = "ALIQUOTA";
        _nomeImposto = nomeImpostoAliquota;
        _taxaFixaNF = taxaFixaNF;
        _taxas = new Hashtable<>();
    }


    //*************************Métodos******************************
    //Configura as aliquotas para cada tipo de categoria tributaria
    public void addOrChangeAliquota(CategoriasTributarias categTribut,
                                    double taxaPorcentagem,
                                    double taxaDefaultIV) {

        if(categTribut != null) {
            _taxas.remove(categTribut);
            _taxas.put(categTribut, new TaxasImpostoAliquota(taxaPorcentagem, taxaDefaultIV));
        }
    }

    //Retorna as aliquotas
    public Dictionary<CategoriasTributarias, TaxasImpostoAliquota> getTaxas() { return _taxas; }

    //Retorna a taxa fixa por NF
    public double getTaxaFixaNF() { return _taxaFixaNF; }

}