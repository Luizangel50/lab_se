package MVP;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luiz Angel on 2/26/2016.
 */
public class View {

    //************************Atributos****************************
    //Presenter associado ao  View
    private Presenter _presenter;


    //*************************Construtor***************************

    protected View(Presenter presenter) { _presenter = presenter; }


    //*************************Métodos******************************
    //Metodo que permite o usuario pedir uma NF
    //com os detalhes do requisito 9
    public void getNFDetalhesAliquotasImpostos(int ID) {
        List<String> dadosDaNF = new ArrayList<>();
        dadosDaNF.add("\n*********************Informacoes Detalhadas Sobre Impostos e Itens de Venda*********************");
        dadosDaNF.add("*********************NOTA FISCAL: ID " + ID + "*********************");
        dadosDaNF.add("\n*****************Itens de Venda*****************");
        List<String> hasNF = _presenter.getNFDetalhesAliquotasImpostos(ID, dadosDaNF);
        if(hasNF.get(0).equals("Nota Fiscal existente")) {

            dadosDaNF.add("\n*********************Resumo*********************");
            dadosDaNF.add("Valor dos Itens de Venda: RS " + hasNF.get(1));
            dadosDaNF.add("Valor Total dos Impostos: RS " + hasNF.get(2));
            dadosDaNF.add("Valor Total da Nota Fiscal: RS " + hasNF.get(3));
        }
        else {
            dadosDaNF.clear();
        }
        printDados(dadosDaNF);
    }


    //Metodo que permite o usuario pedir uma NF
    //com os detalhes do requisito 10
    public void getNFDetalhesItensVenda(int ID) {
        List<String> dadosDaNF = new ArrayList<>();
        dadosDaNF.add("\n*********************Informacoes Detalhadas dos Itens de Venda*********************");
        dadosDaNF.add("*********************NOTA FISCAL: ID " + ID + "*********************");
        dadosDaNF.add("\n*****************Itens de Venda*****************");
        String hasNF = _presenter.getNFDetalhesItensVenda(ID, dadosDaNF);
        if(hasNF.equals("Nota Fiscal inexistente")) {
            dadosDaNF.clear();
        }
        printDados(dadosDaNF);
    }


    //Permite obter infirmacoes sobre NFs
    //com os detalhes do requisito 11
    public void getInformacoesNFs() {
        List<String> dadosDeNFsSistema = new ArrayList<>();
        double[][] dados =  _presenter.getInformacoesNFs();
        dadosDeNFsSistema.add("\n*********************Informacoes Gerais as Notas Fiscais do Sistema*********************");

        for(int index = 0; index < dados.length; index++) {
            dadosDeNFsSistema.add("*********************NOTA FISCAL: ID " + (int)dados[index][0] + "*********************");
            dadosDeNFsSistema.add("Total dos valores dos Itens de Venda: RS " + dados[index][1]);
            dadosDeNFsSistema.add("Soma Total dos Impostos sobre os Itens de Venda: RS " + dados[index][2]);
            if (dados[index][3] != 0) dadosDeNFsSistema.add("Total de impostos fixos: RS " + dados[index][3]);
            dadosDeNFsSistema.add("Valor Final da Nota Fiscal: RS " + dados[index][4]);
        }
        if(dados.length == 0) {
            dadosDeNFsSistema.clear();
            dadosDeNFsSistema.add("Nao ha Notas Fiscais no Sistema");
        }
        printDados(dadosDeNFsSistema);
    }


    //Metodo que imprime dados para o usuario
    private void printDados(List<String> dados) {
        for(String dado : dados) {
            System.out.println(dado);
        }
    }
}
