package MVP;

import NF.BancoNotasFiscais;
import NF.NotaFiscal;
import NF.OperacoesNotasFiscais;

import java.util.List;

/**
 * Created by Luiz Angel on 2/26/2016.
 */
public class Model {

    //************************Atributos****************************


    //*************************Construtor***************************
    protected Model() { }


    //*************************Métodos******************************
    //Metodo que busca as informacoes de uma NF (IVs e Impostos aplicados)
    protected List<String> detalhesNFAliquotasImpostos(NotaFiscal nf) {
        return OperacoesNotasFiscais.buscarDetalhesNFeImpostos(nf);
    }


    //Metodo que busca dados de Itens de Venda e impostos associados a esses IVs
    //Os dados da busca esta na lista dados (passado como parametro do metodo)
    protected void detalhesNFItensVenda(NotaFiscal nf, List<String> dados) {
        OperacoesNotasFiscais.buscarDetalhesNFeIVs(nf, dados);
    }


    //Metodo que busca informacoes resumidas das NFs do sistema
    protected double[][] informacoesNFsDoSistema() {
        return OperacoesNotasFiscais.buscarInformacoesSuperficiaisDeNFs();
    }


    //Retorna um nota fiscal do Banco de Notas Fiscais
    protected NotaFiscal hasNFinBNF(int ID) {
        NotaFiscal notaFiscal = null;
        try {
            for (NotaFiscal nf : BancoNotasFiscais.getInstanceOfBancodeNotasFiscais().getNotasFiscais()) {
                if (nf.getID() == ID) {
                    notaFiscal = nf;
                }
            }
            if (notaFiscal == null) {
                throw new Exception("Nota Fiscal nao escontrada.");
            }
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return notaFiscal;
    }
}
