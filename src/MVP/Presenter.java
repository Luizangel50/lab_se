package MVP;

import IV.ItemVenda;
import NF.NotaFiscal;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luiz Angel on 2/26/2016.
 */
public class Presenter {

    //************************Atributos****************************
    //View associado a este presenter
    private View _view;

    //Model associado a este presenter
    private Model _model;


    //*************************Construtor***************************
    public Presenter() {
        _view = new View(this);
        _model = new Model();
    }


    //*************************Métodos******************************
    //Retorna o View
    public View getView() { return _view; }

    //Procura a NF pelo ID e retorna se ela existe ou nao
    protected NotaFiscal hasNFwithID(int ID) {
        NotaFiscal notaFiscal = _model.hasNFinBNF(ID);
        return notaFiscal;
    }




    //Recolhe dados coletados e organizados pelo Model
    //de acordo com o requisito 9
    protected List<String> getNFDetalhesAliquotasImpostos(int ID, List<String> dadosDaNF) {
        NotaFiscal nf = hasNFwithID(ID);
        List<String> hasNF = new ArrayList<>();
        if(nf != null) {
            for(ItemVenda itemVenda : nf.getItensVenda()) percorrerIVs(itemVenda, dadosDaNF);
            dadosDaNF.addAll(_model.detalhesNFAliquotasImpostos(nf));
            hasNF.add("Nota Fiscal existente");
            hasNF.add(String.valueOf(nf.getValorIVs()));
            hasNF.add(String.valueOf(nf.getValorNF() - nf.getValorIVs()));
            hasNF.add(String.valueOf(nf.getValorNF()));
        }
        else {
           hasNF.add("Nota Fiscal inexistente");
        }
        return hasNF;
    }

    //Metodo auxiliar: Percorre a arvore de IVs, recolhendo dados
    private void percorrerIVs(ItemVenda itemVenda, List<String> dados) {
        dados.add("**Item de Venda: " + itemVenda.getNomeIV() + "**");
        dados.add("Valor do Item de Venda: RS " + itemVenda.getValorIV());
        dados.add("Categoria do Item de Venda: " + itemVenda.getCategTribut().name());
        for(ItemVenda iv : itemVenda.getItensVenda()) percorrerIVs(iv, dados);
    }




    //Recolhe dados coletados e organizados pelo Model
    //de acordo com o requisito 10
    protected String getNFDetalhesItensVenda(int ID, List<String> dadosDaNF) {
        NotaFiscal nf = hasNFwithID(ID);
        if(nf != null) {
            _model.detalhesNFItensVenda(nf, dadosDaNF);
            double impostosFixos = nf.getValoresImpostos()[nf.getValoresImpostos().length - 2];
            if (impostosFixos != 0) dadosDaNF.add("Total Impostos Fixos: RS " + impostosFixos);
            dadosDaNF.add("Valor Final da Nota Fiscal: RS " + nf.getValorNF());
            return "Nota Fiscal existente";
        }
        else {
            return "Nota Fiscal inexistente";
        }
    }




    //Recolhe dados coletados e organizados pelo Model
    //de acordo com o requisito 11
    protected double[][] getInformacoesNFs() {
        List<String> dadosDeNFsSistema = new ArrayList<>();
        double dadosModel[][] = _model.informacoesNFsDoSistema();
        return dadosModel;
    }

}
