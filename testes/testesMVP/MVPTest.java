package testesMVP;

import IV.ItemVenda;
import Imposto.*;
import ClassesAuxiliares.CategoriasTributarias;
import MVP.Presenter;
import NF.NotaFiscal;
import NF.NotaFiscalBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by Luiz Angel on 2/26/2016.
 */
public class MVPTest {

    NotaFiscal notaFiscal1;
    NotaFiscal notaFiscal2;
    NotaFiscal notaFiscal3;
    NotaFiscal notaFiscal4;

    ItemVenda itemVenda1;
    ItemVenda itemVenda2;
    ItemVenda itemVenda3;
    ItemVenda itemVenda4;

    ImpostoAliquota impostoAliquota1;
    ImpostoAliquota impostoAliquota2;
    ImpostoAliquota impostoAliquota3;
    ImpostoAliquota impostoAliquota4;

    ImpostoProgressivo impostoProgressivo1;
    ImpostoProgressivo impostoProgressivo2;
    ImpostoProgressivo impostoProgressivo3;
    ImpostoProgressivo impostoProgressivo4;

    Presenter presenter;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    //MVP
    @Test
    public void testandoCalcularComNFComAlgunsIVs() {
        //Criando imposto aliquota
        impostoAliquota1 = ImpostoAliquotaBuilder.createImpostoAliquota("impostoAliquota1", 1, CategoriasTributarias.Educacao, 20, 7.3);
        impostoAliquota1.addOrChangeAliquota(CategoriasTributarias.Habitacao, 2, 3);

        //Criando imposto progressivo
        impostoProgressivo1 = ImpostoProgressivoBuilder.createImpostoProgressivo("impostoProgressivo1", 10, 50, 1, 2, 5);
        impostoProgressivo1.addCategTribut(CategoriasTributarias.Educacao);
        impostoProgressivo1.addCategTribut(CategoriasTributarias.RendaPessoaFisica);

        //Criando lista com 1 IV
        ArrayList<ItemVenda> itensVenda = new ArrayList<>();
        itensVenda.add(ItemVenda.createItemVenda("item1", CategoriasTributarias.Educacao, 5));

        //Criando NF com 1 IV
        notaFiscal2 = NotaFiscalBuilder.createNewNotaFiscal(itensVenda);

        presenter = new Presenter();

        //presenter.getView().getNF(0);
    }
}