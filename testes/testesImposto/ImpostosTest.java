package testesImposto;

import Imposto.*;
import ClassesAuxiliares.CategoriasTributarias;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Luiz Angel on 2/23/2016.
 */
public class ImpostosTest {

    Imposto imposto1;
    Imposto imposto2;
    ImpostoAliquota impostoAliquota1;
    ImpostoAliquota impostoAliquota2;
    ImpostoProgressivo impostoProgressivo1;
    ImpostoProgressivo impostoProgressivo2;
    BancoImpostos testeBI;

    @Before
    public void setUp() throws Exception {
        testeBI = BancoImpostos.getInstanceOfBancodeImpostos();
    }

    @After
    public void tearDown() throws Exception {
        testeBI.clearBI();
    }

    //Imposto: Caso null
    @Test
    public void testandoImpostoNulo() {
        //Imposto null
        imposto1 = null;
        assertTrue("Imposto Nulo", imposto1 == null);

        //Nao se pode criar Imposto nulo, a partir de algum Builder de Imposto
        imposto1 = ImpostoAliquotaBuilder.createImpostoAliquota("impostoTeste", 3);
        assertFalse("Imposto Nao-Nulo", imposto1 == null);

        //Ao tentar criar imposto com nome nulo, e retornado um imposto nulo
        imposto1 = ImpostoAliquotaBuilder.createImpostoAliquota(null, 1);
        assertTrue("O nome do Imposto nao pode ser null", imposto1 == null);
    }

    //Imposto: Criando Impostos do tipo ALIQUOTA e PROGRESSIVO
    @Test
    public void testandoCriarImpostosAliquotaProgressivoComPolimorfismo() {
        //*****************************COM POLIMORFISMO*****************************
        //Criando impostos
        imposto1 = ImpostoAliquotaBuilder.createImpostoAliquota("imposto1", 3);
        ((ImpostoAliquota) imposto1).addOrChangeAliquota(CategoriasTributarias.Educacao, 1, 2);
        imposto2 = ImpostoProgressivoBuilder.createImpostoProgressivo("imposto2", 10, 100, 4, 5, 6);
        ((ImpostoProgressivo) imposto2).addCategTribut(CategoriasTributarias.Habitacao);

        //Testando os nomes dos impostos
        assertEquals("imposto1", imposto1.getNomeImposto());
        assertEquals("imposto2", imposto2.getNomeImposto());

        //Testando os tipos dos impostos e categorias tributariass
        assertEquals("ALIQUOTA", imposto1.getTipoImposto());
        assertEquals("PROGRESSIVO", imposto2.getTipoImposto());
        assertEquals(CategoriasTributarias.Habitacao, ((ImpostoProgressivo) imposto2).getListaCategTribut().get(0));

        //Testando as taxas e aliquotas
        assertEquals(1, ((ImpostoAliquota) imposto1).getTaxas().get(CategoriasTributarias.Educacao).getTaxaPorcentagem(), 0);
        assertEquals(2, ((ImpostoAliquota) imposto1).getTaxas().get(CategoriasTributarias.Educacao).getTaxaDefaultIV(), 0);
        assertEquals(3, ((ImpostoAliquota) imposto1).getTaxaFixaNF(), 0);

        assertEquals(10, ((ImpostoProgressivo) imposto2).getFaixasAliquotas().getFaixaPiso(), 0);
        assertEquals(100, ((ImpostoProgressivo) imposto2).getFaixasAliquotas().getFaixaTeto(), 0);
        assertEquals(4, ((ImpostoProgressivo) imposto2).getFaixasAliquotas().getAliquotaPiso(), 0);
        assertEquals(5, ((ImpostoProgressivo) imposto2).getFaixasAliquotas().getAliquotaPisoTeto(), 0);
        assertEquals(6, ((ImpostoProgressivo) imposto2).getFaixasAliquotas().getAliquotaTeto(), 0);
    }

    //Imposto: Criando Impostos do tipo ALIQUOTA e PROGRESSIVO
    @Test
    public void testandoCriarImpostosAliquotaProgressivoSemPolimorfismo() {
        //*****************************SEM POLIMORFISMO*****************************
        //Criando impostos
        impostoAliquota1 = ((ImpostoAliquota) ImpostoAliquotaBuilder.createImpostoAliquota("impostoAliquota1", 3));
        impostoProgressivo1 = ((ImpostoProgressivo) ImpostoProgressivoBuilder.createImpostoProgressivo("impostoProgressivo1", 10, 100, 4, 5, 6));
        impostoAliquota1.addOrChangeAliquota(CategoriasTributarias.Habitacao, 1, 2);
        impostoProgressivo1.addCategTribut(CategoriasTributarias.Educacao);

        //Testando os nomes dos impostos
        assertEquals("impostoAliquota1", impostoAliquota1.getNomeImposto());
        assertEquals("impostoProgressivo1", impostoProgressivo1.getNomeImposto());

        //Testando os tipos dos impostos e categorias tributarias
        assertEquals("ALIQUOTA", impostoAliquota1.getTipoImposto());
        assertEquals("PROGRESSIVO", impostoProgressivo1.getTipoImposto());
        assertEquals(CategoriasTributarias.Educacao, impostoProgressivo1.getListaCategTribut().get(0));

        //Testando as taxas e aliquotas
        assertEquals(1, impostoAliquota1.getTaxas().get(CategoriasTributarias.Habitacao).getTaxaPorcentagem(), 0);
        assertEquals(2, impostoAliquota1.getTaxas().get(CategoriasTributarias.Habitacao).getTaxaDefaultIV(), 0);
        assertEquals(3, impostoAliquota1.getTaxaFixaNF(), 0);

        assertEquals(10, impostoProgressivo1.getFaixasAliquotas().getFaixaPiso(), 0);
        assertEquals(100, impostoProgressivo1.getFaixasAliquotas().getFaixaTeto(), 0);
        assertEquals(4, impostoProgressivo1.getFaixasAliquotas().getAliquotaPiso(), 0);
        assertEquals(5, impostoProgressivo1.getFaixasAliquotas().getAliquotaPisoTeto(), 0);
        assertEquals(6, impostoProgressivo1.getFaixasAliquotas().getAliquotaTeto(), 0);
    }

    //Imposto: Comparacao com os Impostos do BI
    @Test
    public void testandoImpostosExistentesPolimorfismoComBI() {
        //*****************************COM POLIMORFISMO*****************************
        //Criando impostos
        imposto1 = ImpostoAliquotaBuilder.createImpostoAliquota("imposto1", 3);
        imposto2 = ImpostoProgressivoBuilder.createImpostoProgressivo("imposto2", 10, 100, 4, 5, 6);
        ((ImpostoProgressivo) imposto2).addCategTribut(CategoriasTributarias.Previdencia);
        ((ImpostoProgressivo) imposto2).addCategTribut(CategoriasTributarias.Importacao);
        List<Imposto> listaImpostos = testeBI.getImpostos();

        //Testando os nomes dos impostos
        assertEquals(imposto1.getNomeImposto(), testeBI.getImpostos().get(0).getNomeImposto());
        assertEquals(imposto2.getNomeImposto(), testeBI.getImpostos().get(1).getNomeImposto());

        //Testando os tipos dos impostos e categorias tributarias
        assertEquals(imposto1.getTipoImposto(), testeBI.getImpostos().get(0).getTipoImposto());
        assertEquals(imposto2.getTipoImposto(), testeBI.getImpostos().get(1).getTipoImposto());
        assertEquals("Categoria Tributaria: Previdencia", ((ImpostoProgressivo) imposto2).getListaCategTribut().get(0),
                ((ImpostoProgressivo)testeBI.getImpostos().get(1)).getListaCategTribut().get(0));
        assertEquals("Categoria Tributaria: Importacao", ((ImpostoProgressivo) imposto2).getListaCategTribut().get(1),
                ((ImpostoProgressivo)testeBI.getImpostos().get(1)).getListaCategTribut().get(1));
        assertEquals("Categoria Tributaria: Importacao", CategoriasTributarias.Importacao,
                ((ImpostoProgressivo)testeBI.getImpostos().get(1)).getListaCategTribut().get(1));
        //System.out.println("Tamanho: " + ((ImpostoProgressivo)testeBI.getImpostos().get(1)).getListaCategTribut().size());

        //Testando as taxas e aliquotas
//        assertEquals(((ImpostoAliquota) imposto1).getTaxas().getTaxaPorcentagem(),
//                ((ImpostoAliquota)testeBI.getImpostos().get(0)).getTaxas().getTaxaPorcentagem(),
//                0);
//        assertEquals(((ImpostoAliquota) imposto1).getTaxas().getTaxaDefaultIV(),
//                ((ImpostoAliquota)testeBI.getImpostos().get(0)).getTaxas().getTaxaDefaultIV(),
//                0);
//        assertEquals(((ImpostoAliquota) imposto1).getTaxas().getTaxaFixaNF(),
//                ((ImpostoAliquota)testeBI.getImpostos().get(0)).getTaxas().getTaxaFixaNF(),
//                0);

        assertEquals(((ImpostoProgressivo) imposto2).getFaixasAliquotas().getFaixaPiso(),
                ((ImpostoProgressivo) testeBI.getImpostos().get(1)).getFaixasAliquotas().getFaixaPiso(),
                0);
        assertEquals(((ImpostoProgressivo) imposto2).getFaixasAliquotas().getFaixaTeto(),
                ((ImpostoProgressivo) testeBI.getImpostos().get(1)).getFaixasAliquotas().getFaixaTeto(),
                0);
        assertEquals(((ImpostoProgressivo) imposto2).getFaixasAliquotas().getAliquotaPiso(),
                ((ImpostoProgressivo) testeBI.getImpostos().get(1)).getFaixasAliquotas().getAliquotaPiso(),
                0);
        assertEquals(((ImpostoProgressivo) imposto2).getFaixasAliquotas().getAliquotaPisoTeto(),
                ((ImpostoProgressivo) testeBI.getImpostos().get(1)).getFaixasAliquotas().getAliquotaPisoTeto(),
                0);
        assertEquals(((ImpostoProgressivo) imposto2).getFaixasAliquotas().getAliquotaTeto(),
                ((ImpostoProgressivo) testeBI.getImpostos().get(1)).getFaixasAliquotas().getAliquotaTeto(),
                0);
    }

    //Imposto: Verificar se as faixas de valores
    //para ImpostoProgressivo sao coerentes
    @Test
    public void testandoFaixasValoresImpostoProgressivo() {
        //Criando impostos progressivos
        impostoProgressivo1 = (ImpostoProgressivo) ImpostoProgressivoBuilder.createImpostoProgressivo("exemploIP1", 1, 2, 0.1, 0.2, 0.3);
        impostoProgressivo2 = (ImpostoProgressivo) ImpostoProgressivoBuilder.createImpostoProgressivo("exemploIP2", 2, 1, 0.1, 0.2, 0.3);

        //impostoProgressivo2 deve ser null
        assertFalse("ImpostoProgressivo nao deve ser null", impostoProgressivo1 == null);
        assertTrue("ImpostoProgressivo deve ser null", impostoProgressivo2 == null);

        //Testando imposto com faixas de valores iguais
        impostoProgressivo1 = (ImpostoProgressivo) ImpostoProgressivoBuilder.createImpostoProgressivo("exemploIP3", 1, 1, 0.1, 0.2, 0.3);
        impostoProgressivo2 = (ImpostoProgressivo) ImpostoProgressivoBuilder.createImpostoProgressivo("exemploIP4", 2, 2, 0.1, 0.1, 0.1);

        assertTrue("ImpostoProgressivo deve ser null", impostoProgressivo1 == null);
        assertFalse("ImpostoProgressivo nao deve ser null", impostoProgressivo2 == null);
    }
}