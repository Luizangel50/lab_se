package Demos;

import IV.ItemVenda;
import Imposto.*;
import ClassesAuxiliares.CategoriasTributarias;
import MVP.Presenter;
import MVP.View;
import NF.NotaFiscal;
import NF.NotaFiscalBuilder;
import NF.OperacoesNotasFiscais;

import java.util.ArrayList;

/**
 * Created by Luiz Angel on 2/26/2016.
 */
public class MockMVP {

    //************************Atributos****************************
    NotaFiscal notaFiscal1;
    NotaFiscal notaFiscal2;
    NotaFiscal notaFiscal3;
    NotaFiscal notaFiscal4;

    ItemVenda itemVenda1;
    ItemVenda itemVenda2;
    ItemVenda itemVenda3;
    ItemVenda itemVenda4;
    ItemVenda itemVenda5;

    ImpostoAliquota impostoAliquota1;
    ImpostoAliquota impostoAliquota2;
    ImpostoAliquota impostoAliquota3;
    ImpostoAliquota impostoAliquota4;

    ImpostoProgressivo impostoProgressivo1;
    ImpostoProgressivo impostoProgressivo2;
    ImpostoProgressivo impostoProgressivo3;
    ImpostoProgressivo impostoProgressivo4;

    Presenter presenter;

    //*************************Construtor***************************
    public MockMVP() { }

    //*************************Métodos******************************
    //Método que realiza operacoes mockadas
    public void executeMock() {
        //Criando imposto aliquota
        impostoAliquota1 = ImpostoAliquotaBuilder.createImpostoAliquota("impostoAliquota1", 1, CategoriasTributarias.Educacao, 20, 7.3);
        impostoAliquota1.addOrChangeAliquota(CategoriasTributarias.Habitacao, 2, 3);

        //Criando imposto progressivo
        impostoProgressivo1 = ImpostoProgressivoBuilder.createImpostoProgressivo("impostoProgressivo1", 8, 20, 1, 2, 5);
        impostoProgressivo1.addCategTribut(CategoriasTributarias.Educacao);
        impostoProgressivo1.addCategTribut(CategoriasTributarias.RendaPessoaFisica);

        //Nota Fiscal 0
        //Criando lista com 4 IVs
        itemVenda1 = ItemVenda.createItemVenda("item01", CategoriasTributarias.Educacao, 5);
        itemVenda2 = ItemVenda.createItemVenda("item02", CategoriasTributarias.Habitacao, 1);
        itemVenda3 = ItemVenda.createItemVenda("item03", CategoriasTributarias.Exportacao, 2);
        itemVenda3.addItemVenda(ItemVenda.createItemVenda("item04", CategoriasTributarias.RendaPessoaFisica, 1));

        ArrayList<ItemVenda> itensVenda0 = new ArrayList<>();

        itensVenda0.add(itemVenda1);
        itensVenda0.add(itemVenda2);
        itensVenda0.add(itemVenda3);

        //Criando NF com a lista de IVs
        notaFiscal1 = NotaFiscalBuilder.createNewNotaFiscal(itensVenda0);



        //Nota Fiscal 1
        //Criando lista com 3 IVs
        itemVenda4 = ItemVenda.createItemVenda("item11", CategoriasTributarias.Educacao, 3);
        itemVenda5 = ItemVenda.createItemVenda("item12", CategoriasTributarias.Habitacao, 1);
        itemVenda5.addItemVenda(ItemVenda.createItemVenda("item13", CategoriasTributarias.Exportacao, 2));
        itemVenda4.addItemVenda(itemVenda5);

        ArrayList<ItemVenda> itensVenda1 = new ArrayList<>();
        itensVenda1.add(itemVenda4);

        //Criando NF com a lista de IVs
        notaFiscal2 = NotaFiscalBuilder.createNewNotaFiscal(itensVenda1);


        //Calculando os impostos de todas as Notas Fiscais
        OperacoesNotasFiscais.calcularImpostoTodasNFs();


        presenter = new Presenter();
        View view = presenter.getView();

        //Saidas para o usuario da NF 0
        view.getNFDetalhesAliquotasImpostos(0);
        view.getNFDetalhesItensVenda(0);

        //Saidas para o usuario da NF 1
        view.getNFDetalhesAliquotasImpostos(1);
        view.getNFDetalhesItensVenda(1);

        //Saidas para o usuario de todas as NFs
        view.getInformacoesNFs();
    }
}
