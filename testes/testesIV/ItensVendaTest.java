package testesIV;

import IV.ItemVenda;
import ClassesAuxiliares.CategoriasTributarias;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by Luiz Angel on 2/24/2016.
 */
public class ItensVendaTest {

    ItemVenda itemVenda1;
    ItemVenda itemVenda2;
    ItemVenda itemVenda3;
    ItemVenda itemVenda4;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    //Item de Venda: Caso null
    @Test
    public void testandoIVNulo() {
        //Caso null
        itemVenda1 = null;
        assertTrue("IV null", itemVenda1 == null);

        //Nome nulo
        String nome = null;
        itemVenda1 = ItemVenda.createItemVenda(nome, CategoriasTributarias.Exportacao, 0);
        assertTrue("IV null", itemVenda1 == null);

        //Categoria Tributaria nula
        CategoriasTributarias categ = null;
        itemVenda2 = ItemVenda.createItemVenda("itemVenda2", categ, 1);
        assertEquals("itemVenda2", itemVenda2.getNomeIV());
        assertEquals(1, itemVenda2.getValorIV(), 0);
        assertTrue("Categoria Tributaria nula", itemVenda2.getCategTribut() == null);
        assertTrue("Lista de IVs vazia", itemVenda2.getItensVenda().isEmpty());

        //Lista de IVs nula
        ArrayList<ItemVenda> itemVendas = null;
        itemVenda3 = ItemVenda.createItemVenda("itemVenda3", null, itemVendas, 2);
        assertEquals("itemVenda3", itemVenda3.getNomeIV());
        assertEquals(2, itemVenda3.getValorIV(), 0);
        assertTrue("Categoria Tributaria nula", itemVenda3.getCategTribut() == null);
        assertTrue("Lista de IVs vazia", itemVenda3.getItensVenda().isEmpty());

        //Categoria Tributaria e lista de IVs nulas
        itemVenda4 = ItemVenda.createItemVenda("itemVenda4", categ, itemVendas, 3);
        assertEquals("itemVenda4", itemVenda4.getNomeIV());
        assertEquals(3, itemVenda4.getValorIV(), 0);
        assertTrue("Categoria Tributaria nula", itemVenda4.getCategTribut() == null);
        assertTrue("Lista de IVs vazia", itemVenda4.getItensVenda().isEmpty());
    }

    //Item de Venda: Caso vazio
    @Test
    public void testandoIVComComponenteVazio() {
        //Lista de IVs vazia
        ArrayList<ItemVenda> itemVendas = new ArrayList<ItemVenda>();
        itemVenda1 = ItemVenda.createItemVenda("itemVenda1", CategoriasTributarias.Exportacao, itemVendas, 0);
        assertEquals("itemVenda1", itemVenda1.getNomeIV());
        assertEquals(0, itemVenda1.getValorIV(), 0);
        assertFalse("Categoria Tributaria nula", itemVenda1.getCategTribut() == null);
        assertTrue("Lista de IVs vazia", itemVenda1.getItensVenda().isEmpty());

        //Categoria Tributaria normal e lista de IVs vazia
        itemVenda2 = ItemVenda.createItemVenda("itemVenda22222", CategoriasTributarias.Educacao, itemVendas, 7);
        assertEquals("itemVenda22222", itemVenda2.getNomeIV());
        assertEquals(7, itemVenda2.getValorIV(), 0);
        assertEquals("Categoria Tributaria nao nula", CategoriasTributarias.Educacao, itemVenda2.getCategTribut());
        assertTrue("Lista de IVs vazia", itemVenda2.getItensVenda().isEmpty());
    }

    //Item de Venda: Casos nao-nulo e nao-vazio
    @Test
    public void testandoIVNaoNullNaoVazio() {
        //Criando IVs
        itemVenda1 = ItemVenda.createItemVenda("itemVenda111", CategoriasTributarias.RendaPessoaJuridica, 8);
        itemVenda2 = ItemVenda.createItemVenda("itemVenda222", CategoriasTributarias.Educacao,9);

        //Criando lista de IVs
        ArrayList<ItemVenda> itensVenda1 = new ArrayList<ItemVenda> ();
        ArrayList<ItemVenda> itensVenda2 = new ArrayList<ItemVenda> ();
        itensVenda1.add(itemVenda1);
        itensVenda1.add(itemVenda2);
        itensVenda2.add(itemVenda2);
        itensVenda2.add(itemVenda1);

        itemVenda3 = ItemVenda.createItemVenda("itemVenda333", CategoriasTributarias.Transporte, itensVenda1, 10);
        itemVenda4 = ItemVenda.createItemVenda("itemVenda444", CategoriasTributarias.Habitacao, itensVenda2, 11);

        //Testando IVs
        assertEquals("itemVenda333", itemVenda3.getNomeIV());
        assertEquals("itemVenda444", itemVenda4.getNomeIV());

        assertEquals("itemVenda111", itemVenda3.getItensVenda().get(0).getNomeIV());
        assertNotEquals(null, itemVenda3.getItensVenda().get(0).getCategTribut());
        assertEquals(true, itemVenda3.getItensVenda().get(0).getItensVenda().isEmpty());

        assertEquals("itemVenda222", itemVenda3.getItensVenda().get(1).getNomeIV());
        assertEquals(CategoriasTributarias.Educacao, itemVenda3.getItensVenda().get(1).getCategTribut());
        assertEquals(true, itemVenda3.getItensVenda().get(1).getItensVenda().isEmpty());

        assertEquals("itemVenda333", itemVenda3.getNomeIV());
        assertNotEquals(null, itemVenda3.getCategTribut());
        assertEquals("itemVenda111", itemVenda3.getItensVenda().get(0).getNomeIV());
        assertEquals("itemVenda222", itemVenda3.getItensVenda().get(1).getNomeIV());
        assertEquals(2, itemVenda3.getItensVenda().size());

        assertEquals("itemVenda444", itemVenda4.getNomeIV());
        assertEquals(CategoriasTributarias.Habitacao,  itemVenda4.getCategTribut());
        assertEquals("itemVenda111", itemVenda4.getItensVenda().get(1).getNomeIV());
        assertEquals("itemVenda222", itemVenda4.getItensVenda().get(0).getNomeIV());
        assertEquals(2, itemVenda4.getItensVenda().size());

        //Usando metodo addItemVenda
        itemVenda3.addItemVenda(itemVenda1);
        assertEquals("itemVenda111", itemVenda3.getItensVenda().get(2).getNomeIV());
        assertEquals(3, itemVenda3.getItensVenda().size());

        //Tentando adicionar IV nula
        itemVenda4.addItemVenda(null);
        assertEquals(2, itemVenda4.getItensVenda().size());

        //Mudando a Categoria Tributaria
        itemVenda1.setCategTribut(CategoriasTributarias.Previdencia);
        assertEquals(CategoriasTributarias.Previdencia, itemVenda3.getItensVenda().get(0).getCategTribut());
        assertEquals(CategoriasTributarias.Previdencia, itemVenda3.getItensVenda().get(2).getCategTribut());
        assertEquals(CategoriasTributarias.Previdencia, itemVenda1.getCategTribut());
    }
}
