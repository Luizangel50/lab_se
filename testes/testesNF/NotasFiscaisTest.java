package testesNF;

import NF.BancoNotasFiscais;
import NF.NotaFiscal;
import NF.NotaFiscalBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Luiz Angel on 2/21/2016.
 */
public class NotasFiscaisTest {

    NotaFiscal notaFiscal0;
    NotaFiscal notaFiscal1;
    NotaFiscal notaFiscal2;
    NotaFiscal notaFiscal3;
    BancoNotasFiscais testeBNF;

    @Before
    public void setUp() throws Exception {
        testeBNF = BancoNotasFiscais.getInstanceOfBancodeNotasFiscais();
    }

    @After
    public void tearDown() throws Exception {
        testeBNF.clearBNF();
    }

    //Nota Fiscal: Caso null
    @Test
    public void testandoNotaFiscalNula() {
        //NF nula
        notaFiscal0  = null;
        assertTrue("Nota Fiscal Nula", notaFiscal0 == null);

        //Nao se pode criar, a partir do NFBuilder, NF nula
        notaFiscal1 = NotaFiscalBuilder.createNewNotaFiscal();
        assertFalse("Nota Fiscal Nao-Nula", notaFiscal1 == null);
    }

    //Nota Fiscal: Criando NFs
    @Test
    public void testandoCriarNotasFiscaisEIDsDeNFs() {
        //Criando NF
        notaFiscal0 = NotaFiscalBuilder.createNewNotaFiscal();
        notaFiscal1 = NotaFiscalBuilder.createNewNotaFiscal();

        //Testando o valor do ID
        assertEquals("Deve retornar ID 0", notaFiscal1.getID()-1, notaFiscal0.getID());
    }

    //Nota Fiscal: Comparação com as NFs do BNF
    @Test
    public void testandoNFsExistentesComBNF() {
        //Verificando se as NFs criadas estão no BNF
        notaFiscal2 = NotaFiscalBuilder.createNewNotaFiscal();
        notaFiscal3 = NotaFiscalBuilder.createNewNotaFiscal();

        List<NotaFiscal> nfsBNF = testeBNF.getNotasFiscais();

        //Testando 1a NF
        assertEquals("Deve retornar ID 0", notaFiscal2.getID(), nfsBNF.get(0).getID());

        //Testando 1a NF
        assertEquals ("Deve retornar ID 1", notaFiscal3.getID(), nfsBNF.get(1).getID());
    }

    //Banco de Notas Fiscais: NFs do BNF não podem ser mudadas 
    @Test
    public void testandoNotasFiscaisDoBNF() {
        //Verificando NFs já criadas
        notaFiscal0 = NotaFiscalBuilder.createNewNotaFiscal();
        notaFiscal1 = NotaFiscalBuilder.createNewNotaFiscal();

        //Size da lista antes do teste
        assertEquals("2 NFs", 2, testeBNF.getNotasFiscais().size());

        //Tentar adicionar uma NF
        try {
            testeBNF.getNotasFiscais().add(notaFiscal0);
            assertEquals("3 NFs", 3, testeBNF.getNotasFiscais().size());
            System.out.println("******Teste - Banco de Notas Fiscais: NFs do BNF nao podem ser mudadas (adicao)******");
            System.out.println("Tamanho do BNF 'e " + testeBNF.getNotasFiscais().size() + "; Tamanho alterado.");
        }
        catch (Exception e) {
            assertEquals("2 NFs", 2, testeBNF.getNotasFiscais().size());
            System.out.println("******Teste - Banco de Notas Fiscais: NFs do BNF nao podem ser mudadas (adicao)******");
            System.out.println("Tamanho do BNF 'e " + testeBNF.getNotasFiscais().size() + "; Tamanho nao alterado.");
        }

        //Tentar excluir as NFs
        try {
            testeBNF.getNotasFiscais().clear();
            assertEquals("0 NFs", 0, testeBNF.getNotasFiscais().size());
            System.out.println("******Teste - Banco de Notas Fiscais: NFs do BNF nao podem ser mudadas (exclusao)******");
            System.out.println("Tamanho do BNF 'e " + testeBNF.getNotasFiscais().size() + "; Tamanho alterado.");
        }
        catch (Exception e) {
            assertEquals("2 NFs", 2, testeBNF.getNotasFiscais().size());
            System.out.println("******Teste - Banco de Notas Fiscais: NFs do BNF nao podem ser mudadas (exclusao)******");
            System.out.println("Tamanho do BNF 'e " + testeBNF.getNotasFiscais().size() + "; Tamanho nao alterado.");
        }
    }
}